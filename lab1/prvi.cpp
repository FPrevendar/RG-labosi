#include <stdio.h>
#include <GL/freeglut.h>
#include <vector>
#include <iostream>
#include <glm/glm.hpp>
#include <fstream>
#include <algorithm>
#include <math.h>

/*
*   g++ -Iinclude -c -o prvi.o  prvi.cpp
*   g++ -Llib -o prvi.exe prvi.o -lfreeglut -lglu32 -lOpenGL32 
*   ./prvi.exe -s <imeScene> -k <imeKrivulje>
*/

//*********************************************************************************
//	Pokazivac na glavni prozor i pocetna velicina.
//*********************************************************************************

#define MATH_PI 3.14159265358979323846264338327950288
#define degToRad(kutUStupnjevima) ((kutUStupnjevima)*MATH_PI / 180.0)
#define radToDeg(kutURadijanima) ((kutURadijanima)*180.0 / MATH_PI)

GLuint window;
GLuint width = 300, height = 300;

std::vector<glm::vec3> kontrolneTocke;

glm::mat4x4 matricaBi3 = glm::mat4x4(glm::vec4(-1, 3, -3, 1),
                                     glm::vec4(3, -6, 0, 4),
                                     glm::vec4(-3, 3, 3, 1),
                                     glm::vec4(1, 0, 0, 0));
// puni po stupcima

#define MAX_DULJINA_NAZIVA 100
char *imeDatotekeKrivulje = NULL;
char *imeDatotekeScene = NULL;
char nazivDatotekeKrivulje[MAX_DULJINA_NAZIVA];
char nazivDatotekeScene[MAX_DULJINA_NAZIVA];

std::vector<GLdouble> tockeX;
std::vector<GLdouble> tockeY;
std::vector<GLdouble> tockeZ;
int brojVrhova = 0;

std::vector<int> prviVrh;
std::vector<int> drugiVrh;
std::vector<int> treciVrh;
int brojTrokuta = 0;

double velicinaObjekta = 10;

glm::vec3 pocetnaOrijentacija = glm::vec3(0, 0, 1);

int trenutniSegment = 1;
double parametarT = 0;
double korakAnimacije = 0.01;
int stop = 0;
int prikazTangenta = 1;
int prikazSmjerova = 1;
int prikazKrivulje = 1;

//*********************************************************************************
//	Function Prototypes.
//*********************************************************************************

void myDisplay();
void myReshape(int width, int height);
void myMouse(int button, int state, int x, int y);
void myKeyboard(unsigned char theKey, int mouseX, int mouseY);

void crtaj();
void ucitajKrivulju();
void ucitajScenu();
void postaviInicijalno();
void animate(int);

int main(int argc, char **argv)
{

    for (int argument = 1; argument < argc; argument++)
    {
        if (!strcmp("-k", argv[argument]) && argument < argc + 1)
        {
            imeDatotekeKrivulje = argv[argument + 1];
            argument++;
            std::cout << "Koristim datoteku " << imeDatotekeKrivulje << " kao krivulju." << std::endl;
        }
        else if (!strcmp("-s", argv[argument]) && argument < argc + 1)
        {
            imeDatotekeScene = argv[argument + 1];
            argument++;
            std::cout << "Koristim datoteku " << imeDatotekeScene << " kao scenu." << std::endl;
        }
        else
        {
            std::cout << "Nepoznata opcija " << argv[argument] << " na poziciji " << argument << std::endl;
        }
    }

    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
    glutInitWindowSize(width, height);
    glutInitWindowPosition(100, 100);
    glutInit(&argc, argv);

    //dodati stvari
    if (imeDatotekeKrivulje != NULL)
    {
        ucitajKrivulju();
    }
    else
    {
        //ovo je ona spirala, ovo se izvodi samo ako nije navedena datoteka
        kontrolneTocke.push_back(glm::vec3(0, 0, 0));
        kontrolneTocke.push_back(glm::vec3(0, 10, 5));
        kontrolneTocke.push_back(glm::vec3(10, 10, 10));
        kontrolneTocke.push_back(glm::vec3(10, 0, 15));
        kontrolneTocke.push_back(glm::vec3(0, 0, 20));
        kontrolneTocke.push_back(glm::vec3(0, 10, 25));
        kontrolneTocke.push_back(glm::vec3(10, 10, 30));
        kontrolneTocke.push_back(glm::vec3(10, 0, 35));
        kontrolneTocke.push_back(glm::vec3(0, 0, 40));
        kontrolneTocke.push_back(glm::vec3(0, 10, 45));
        kontrolneTocke.push_back(glm::vec3(10, 10, 50));
        kontrolneTocke.push_back(glm::vec3(10, 0, 55));
    }

    if (imeDatotekeScene != NULL)
    {
        ucitajScenu();
        postaviInicijalno();
    }

    window = glutCreateWindow("Prvi labos");
    glutReshapeFunc(myReshape);
    glutDisplayFunc(myDisplay);
    glutMouseFunc(myMouse);
    glutKeyboardFunc(myKeyboard);
    glutTimerFunc(16, animate, 0);

    glutMainLoop();
    return 0;
}

void myDisplay(void)
{
    glEnable(GL_DEPTH_TEST);
    // printf("Pozvan myDisplay()\n");
    //glClearColor(1,1,1,0);
    glClearColor((double)135 / 255, (double)206 / 255, (double)235 / 255, 0.0f);
    // glClearColor(0,0,0,0); //cnra pozadina
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glPointSize(1.0);
    glColor3f(0.0f, 0.0f, 0.0f);
    crtaj();
    glutSwapBuffers(); // iscrtavanje iz dvostrukog spemnika (umjesto glFlush)
}

void myReshape(int w, int h)
{
    //printf("Pozvan myReshape()\n");
    width = w;
    height = h;                      //promjena sirine i visine prozora
    glViewport(0, 0, width, height); //  otvor u prozoru
    //glClearColor(1,1,1,0);
    glClearColor((double)135 / 255, (double)206 / 255, (double)235 / 255, 0.0f); // boja pozadine
    //glClearColor(0,0,0,0); //crna pozadina
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); //	brisanje pozadine
    glPointSize(1.0);                                   //	postavi velicinu tocke za liniju
    glColor3f(0.0f, 0.0f, 0.0f);                        //	postavi boju linije
    glMatrixMode(GL_PROJECTION);                        // aktivirana matrica projekcije
    glLoadIdentity();
    gluPerspective(45.0, (float)width / height, 0.5, 1000.0);
    // kut pogleda, x/y, prednja i straznja ravnina odsjecanja
    glMatrixMode(GL_MODELVIEW); // aktivirana matrica modela
    glLoadIdentity();
    gluLookAt(60, 60, 30,
              0.0, 0.0, 25.0,
              0.0, 0.0, 1.0);
    // ociste x,y,z
    // glediste x,y,z
    // up vektor x,y,z
}

void myMouse(int button, int state, int x, int y)
{
    //dodati
}

void myKeyboard(unsigned char theKey, int mouseX, int mouseY)
{
    switch (theKey)
    {
    case 'r':
        trenutniSegment = 1;
        parametarT = 0;
        break;
    case 's':
        stop = 1 - stop;
        if (stop == 0)
        {
            glutTimerFunc(16, animate, 0);
        }
        break;
    case 't':
        prikazTangenta = 1 - prikazTangenta;
        glutPostRedisplay();
        break;
    case 'f':
        prikazKrivulje = 1 - prikazKrivulje;
        glutPostRedisplay();
        break;
    case 'g':
        prikazSmjerova = 1 - prikazSmjerova;
        glutPostRedisplay();
        break;
    }
    glutPostRedisplay();
}

void crtaj()
{

    //pod
    glColor3f((double)77 / 255, (double)111 / 255, (double)57 / 255);
    glBegin(GL_TRIANGLE_FAN);
    glVertex3i(-100, -100, 0);
    glVertex3i(-100, 100, 0);
    glVertex3i(100, 100, 0);
    glVertex3i(100, -100, 0);
    glEnd();

    /* //kontrolni poligon
    glColor3f(0, 0, 0);
    glBegin(GL_LINE_STRIP);
    for (auto tocka : kontrolneTocke)
    {
        glVertex3f(tocka.x, tocka.y, tocka.z);
    }
    glEnd();
    */

    for (int i = 1; i < kontrolneTocke.size() - 2; i++)
    {
        glm::mat3x4 matricaRi = glm::transpose(glm::mat4x3(kontrolneTocke[i - 1],
                                                           kontrolneTocke[i],
                                                           kontrolneTocke[i + 1],
                                                           kontrolneTocke[i + 2]));
        //puni po stupcima pa treba transponirati
        if (prikazKrivulje)
        {
            glBegin(GL_LINE_STRIP);
            glColor3f(0, 0, 0);
            for (double t = 0; t <= 1; t += 0.01)
            {
                glm::vec4 parametar = glm::vec4(t * t * t, t * t, t, 1);
                glm::vec3 rez = parametar * ((float)1 / 6) * matricaBi3 * matricaRi;
                glVertex3f(rez.x, rez.y, rez.z);
                glm::vec4 parametarDerivacije = glm::vec4(3 * t * t, 2 * t, 1, 0);
                glm::vec3 derivacija = parametarDerivacije * ((float)1 / 2) * matricaBi3 * matricaRi;
            }
            glEnd();
        }
        if (prikazTangenta)
        {
            //glColor3f(0, 1, 1);
            glColor3f(1, 0, 0);
            for (double t = 0; t <= 1; t += 0.2)
            {
                glm::vec4 parametar = glm::vec4(t * t * t, t * t, t, 1);
                glm::vec3 rez = parametar * ((float)1 / 6) * matricaBi3 * matricaRi;
                glm::vec4 parametarDerivacije = glm::vec4(3 * t * t, 2 * t, 1, 0);
                glm::vec3 derivacija = parametarDerivacije * ((float)1 / 2) * matricaBi3 * matricaRi;
                glm::vec3 linija = rez + (float)5 * glm::normalize(derivacija);
                glBegin(GL_LINES);
                glVertex3f(rez.x, rez.y, rez.z);
                glVertex3f(linija.x, linija.y, linija.z);
                glEnd();
            }
        }
    }

    //crtanje
    glm::mat3x4 matricaRi = glm::transpose(glm::mat4x3(kontrolneTocke[trenutniSegment - 1],
                                                       kontrolneTocke[trenutniSegment],
                                                       kontrolneTocke[trenutniSegment + 1],
                                                       kontrolneTocke[trenutniSegment + 2]));
    glm::vec4 parametar = glm::vec4(parametarT * parametarT * parametarT,
                                    parametarT * parametarT,
                                    parametarT,
                                    1);
    glm::vec3 rez = parametar * ((float)1 / 6) * matricaBi3 * matricaRi;
    glm::vec4 parametarDerivacije = glm::vec4(3 * parametarT * parametarT,
                                              2 * parametarT,
                                              1,
                                              0);
    glm::vec3 derivacija = parametarDerivacije * ((float)1 / 2) * matricaBi3 * matricaRi;
    glm::vec3 osRotacije = glm::cross(pocetnaOrijentacija, derivacija);
    double cosKut = glm::dot(pocetnaOrijentacija, derivacija) / (glm::length(pocetnaOrijentacija) * glm::length(derivacija));
    double kut = radToDeg(acos(cosKut));

    glPushMatrix();
    glTranslatef(rez.x, rez.y, rez.z);
    glRotatef(kut, osRotacije.x, osRotacije.y, osRotacije.z);
    for (int i = 0; i < brojTrokuta; i++)
    {
        glColor3f(0.5, 0, 0.5);
        glBegin(GL_TRIANGLES);
        glVertex3f(tockeX[prviVrh[i]], tockeY[prviVrh[i]], tockeZ[prviVrh[i]]);
        glVertex3f(tockeX[drugiVrh[i]], tockeY[drugiVrh[i]], tockeZ[drugiVrh[i]]);
        glVertex3f(tockeX[treciVrh[i]], tockeY[treciVrh[i]], tockeZ[treciVrh[i]]);
        glEnd();

        if (prikazSmjerova)
        {
            glBegin(GL_LINES);
            glColor3f(1, 0, 0);
            glVertex3f(0, 0, 0);
            glVertex3f(velicinaObjekta, 0, 0);
            glColor3f(0, 1, 0);
            glVertex3f(0, 0, 0);
            glVertex3f(0, velicinaObjekta, 0);
            glColor3f(0, 0, 1);
            glVertex3f(0, 0, 0);
            glVertex3f(0, 0, velicinaObjekta);
            glEnd();
        }
    }
    glPopMatrix();
    /*
    for (int i = 0; i < brojTrokuta; i++)
    {
        glColor3f(0.5, 0, 0.5);
        glBegin(GL_TRIANGLES);
        glVertex3f(tockeX[prviVrh[i]], tockeY[prviVrh[i]], tockeZ[prviVrh[i]]);
        glVertex3f(tockeX[drugiVrh[i]], tockeY[drugiVrh[i]], tockeZ[drugiVrh[i]]);
        glVertex3f(tockeX[treciVrh[i]], tockeY[treciVrh[i]], tockeZ[treciVrh[i]]);
        glEnd();
    }
    */
}

void ucitajKrivulju()
{
    std::ifstream ulaz(imeDatotekeKrivulje);
    double x, y, z;
    while (ulaz >> x >> y >> z)
    {
        kontrolneTocke.push_back(glm::vec3(x, y, z));
    }
    ulaz.close();
}

void ucitajScenu()
{
    FILE *datoteka = fopen(imeDatotekeScene, "r");
    char znak;
    GLint a, b, c;
    GLdouble x, y, z;
    if (datoteka == NULL)
    {
        printf("Ne mogu otvoritit datoteku\n");
        exit(0);
    }
    znak = fgetc(datoteka);
    while (!feof(datoteka))
    {
        if (znak == '#')
        {
            fscanf(datoteka, "%*[^\n]\n");
            /*printf("Nasao komentar.\n");*/
        }
        else if (znak == 'v')
        {
            fscanf(datoteka, " %lf %lf %lf\n", &x, &y, &z);
            tockeX.push_back(x);
            tockeY.push_back(y);
            tockeZ.push_back(z);
            brojVrhova++;
            /*printf("Nasao vrh, %lf, %lf, %lf.\n", x, y, z);*/
        }
        else if (znak == 'f')
        {
            fscanf(datoteka, " %d %d %d\n", &a, &b, &c);
            /*printf("Nasao trokut %d, %d, %d.\n", a, b, c);*/
            prviVrh.push_back(--a); /*Stavljam -1 da mi je prvi 0 a ne 1 */
            drugiVrh.push_back(--b);
            treciVrh.push_back(--c);
            brojTrokuta++;
        }
        else
        {
            /*Nasao nelegalan znak na pocetku redka .obj datoteke */
            printf("%c\n", znak);
            fscanf(datoteka, "%*[^\n]\n");
        }
        znak = fgetc(datoteka);
    }
    printf("Pronasao %d vrhova i %d trokuta.\n", brojVrhova, brojTrokuta);
    fclose(datoteka);
}

void postaviInicijalno()
{
    int i;
    GLdouble xRaspon, yRaspon, zRaspon;
    GLdouble najveciRaspon;
    GLdouble koeficjentSkaliranja;

    auto xmax = *std::max_element(tockeX.begin(), tockeX.end());
    auto xmin = *std::min_element(tockeX.begin(), tockeX.end());
    auto ymax = *std::max_element(tockeY.begin(), tockeY.end());
    auto ymin = *std::min_element(tockeY.begin(), tockeY.end());
    auto zmax = *std::max_element(tockeZ.begin(), tockeZ.end());
    auto zmin = *std::min_element(tockeZ.begin(), tockeZ.end());

    xRaspon = xmax - xmin;
    yRaspon = ymax - ymin;
    zRaspon = zmax - zmin;

    if (xRaspon >= yRaspon)
    {
        najveciRaspon = xRaspon;
    }
    else
    {
        najveciRaspon = yRaspon;
    }
    if (zRaspon > najveciRaspon)
    {
        najveciRaspon = zRaspon;
    }

    koeficjentSkaliranja = (double)1 / najveciRaspon * velicinaObjekta;

    auto xSrediste = (xmax + xmin) / 2;
    auto ySrediste = (ymax + ymin) / 2;
    auto zSrediste = (zmax + zmin) / 2;

    /*Translacija da je srediste u (0,0,0) */
    for (i = 0; i < brojVrhova; i++)
    {
        tockeX[i] = tockeX[i] - xSrediste;
        tockeY[i] = tockeY[i] - ySrediste;
        tockeZ[i] = tockeZ[i] - zSrediste;
    }

    for (i = 0; i < brojVrhova; i++)
    {
        tockeX[i] = tockeX[i] * koeficjentSkaliranja;
        tockeY[i] = tockeY[i] * koeficjentSkaliranja;
        tockeZ[i] = tockeZ[i] * koeficjentSkaliranja;
    }
}

void animate(int value)
{
    if (stop)
    {
        return;
    }
    if (parametarT + korakAnimacije <= 1)
    {
        parametarT += korakAnimacije;
        glutPostRedisplay();
        glutTimerFunc(16, animate, 0);
    }
    else if (trenutniSegment + 1 < kontrolneTocke.size() - 2)
    {
        parametarT = 0;
        trenutniSegment += 1;
        glutPostRedisplay();
        glutTimerFunc(16, animate, 0);
    }
    else
    {
        stop = 1;
    }
}