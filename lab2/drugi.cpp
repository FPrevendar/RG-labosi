#include <stdio.h>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <vector>
#include <iostream>
#include <chrono>
#include <random>
#include <algorithm>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#define MATH_PI 3.14159265358979323846264338327950288
#define degToRad(kutUStupnjevima) ((kutUStupnjevima)*MATH_PI / 180.0)
#define radToDeg(kutURadijanima) ((kutURadijanima)*180.0 / MATH_PI)

inline double random_double()
{
    static std::uniform_real_distribution<double> distribution(0.0, 1.0);
    static std::mt19937 generator;
    return distribution(generator);
}

inline double random_double(double min, double max)
{
    return min + (max - min) * random_double();
}

void myDisplay();
void myReshape(int width, int height);
void myKeyboard(unsigned char theKey, int mouseX, int mouseY);
void iduciFrame(int);
void init();
void mouse_func(int button, int state, int x, int y);
void motion_func(int x, int y);

void updatePerspective(int, int);

void crtaj();

void zoom(int);

void drawCestica();
void updateCestica(double);

GLuint vertexShader;
GLuint fragmentShader;
GLuint shaderProgram;

struct Kamera
{
    GLfloat ocisteX;
    GLfloat ocisteY;
    GLfloat ocisteZ;

    GLfloat gledisteX;
    GLfloat gledisteY;
    GLfloat gledisteZ;

    GLfloat vUpX;
    GLfloat vUpY;
    GLfloat vUpZ;
};

struct Cestica
{
    GLfloat pozx;
    GLfloat pozy;
    GLfloat pozz;

    GLfloat vx;
    GLfloat vy;
    GLfloat vz;

    GLfloat zivot;
};

class Prozor
{
public:
    GLuint windowID;
    GLuint width;
    GLuint height;
};


class Model
{
public:
    virtual void prepare() = 0;
    virtual void draw() = 0;
    virtual void clean() = 0;
};

class SceneGraphNode
{
public:
    virtual void prepare() = 0;
    virtual void update(double t) = 0;
    virtual void draw() = 0;
    virtual void addChild(SceneGraphNode *child) = 0;
    virtual void cleanUp() = 0;
};

class AbstractSceneGraphNode : public SceneGraphNode
{
protected:
    std::vector<SceneGraphNode *> children = std::vector<SceneGraphNode *>();

public:
    virtual void prepare()
    {
        for (auto child : children)
        {
            child->prepare();
        }
    };
    virtual void update(double t)
    {
        for (auto child : children)
        {
            child->update(t);
        }
    }
    virtual void draw()
    {
        for (auto child : children)
        {
            child->draw();
        }
    }
    virtual void addChild(SceneGraphNode *child)
    {
        children.push_back(child);
    }

    virtual void cleanUp()
    {
        for (auto child : children)
        {
            child->cleanUp();
        }
    }
};

class RootSceneGraphNode : public AbstractSceneGraphNode
{
    //nista posebno
};

class ModelSceneGraphNode : public AbstractSceneGraphNode
{
public:
    ModelSceneGraphNode(Model *m)
    {
        this->model = m;
    }
    virtual void prepare()
    {
        model->prepare();
    }
    virtual void update(double t){};

    virtual void draw()
    {
        model->draw();
    }
    virtual void addChild(SceneGraphNode *child) {}

    virtual void cleanUp()
    {
        model->clean();
    }

private:
    Model *model;
};

class TranslateSceneGraphNode : public AbstractSceneGraphNode
{
public:
    TranslateSceneGraphNode(GLfloat x, GLfloat y, GLfloat z)
    {
        this->x = x;
        this->y = y;
        this->z = z;
    }

    virtual void draw()
    {
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glTranslatef(x, y, z);
        for (auto child : children)
        {
            child->draw();
        }
        glPopMatrix();
    }

private:
    GLfloat x, y, z;
};

class RotatedSceneGraphNode : public AbstractSceneGraphNode
{
public:
    RotatedSceneGraphNode(GLfloat x, GLfloat y, GLfloat z, GLfloat fi)
    {
        this->x = x;
        this->y = y;
        this->z = z;
        this->fi = fi;
    }

    virtual void draw()
    {
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glRotatef(fi, x, y, z);
        for (auto child : children)
        {
            child->draw();
        }
        glPopMatrix();
    }

private:
    GLfloat x, y, z, fi;
};

class RotatedAnimatedSceneGraphNode : public AbstractSceneGraphNode
{
public:
    RotatedAnimatedSceneGraphNode(GLfloat x, GLfloat y, GLfloat z, GLfloat fi, GLfloat omega)
    {
        this->x = x;
        this->y = y;
        this->z = z;
        this->fi = fi;
        this->omega = omega;
    }

    virtual void draw()
    {
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glRotatef(fi, x, y, z);
        for (auto child : children)
        {
            child->draw();
        }
        glPopMatrix();
    }

    virtual void update(double t){
        fi -= t * omega;
        if(fi > 360){
            fi = 0;
        }
        for(auto child: children){
            child->update(t);
        }

    }

private:
    GLfloat x, y, z, fi, omega;
};

class ScaledSceneGraphNode : public AbstractSceneGraphNode
{
public:
    ScaledSceneGraphNode(GLfloat x, GLfloat y, GLfloat z)
    {
        this->x = x;
        this->y = y;
        this->z = z;
    }

    virtual void draw()
    {
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glScalef(x, y, z);
        for (auto child : children)
        {
            child->draw();
        }
        glPopMatrix();
    }

private:
    GLfloat x, y, z;
};

class Podloga : public Model
{
public:
    virtual void prepare()
    {
        glGenVertexArrays(1, &vaoId);
        glBindVertexArray(vaoId);
        glGenBuffers(1, &vboId);
        glBindBuffer(GL_ARRAY_BUFFER, vboId);
        glBufferData(GL_ARRAY_BUFFER, 18 * sizeof(GLfloat), vrhovi, GL_STATIC_DRAW);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void *)0);
        glEnableVertexAttribArray(0);
        glBindVertexArray(0);
    }

    virtual void draw()
    {
        glColor3f((float)77 / 255, (float)111 / 255, (float)57 / 255);
        glUniform4f(glGetUniformLocation(shaderProgram, "color"), (float)77 / 255, (float)111 / 255, (float)57 / 255, 1);
        glBindVertexArray(vaoId);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        glBindVertexArray(0);
    }

    virtual void clean()
    {
        glDeleteBuffers(1, &vboId);
        glDeleteVertexArrays(1, &vaoId);
    }

private:
    GLuint vaoId;
    GLuint vboId;
    GLfloat vrhovi[12] = {10.0, 0.0, 10.0,
                          10.0, 0.0, -10.0,
                          -10.0, 0.0, -10.0,
                          -10.0, 0.0, 10.0};
};

class UcitaniModel : public Model
{
public:
    UcitaniModel(){};
    UcitaniModel(const char *imeDatoteke)
    {
        std::cout << "Ucitavam " << imeDatoteke << "\n";
        vrhovi = std::vector<GLfloat>();
        trokuti = std::vector<GLuint>();
        brojTrokuta = 0;
        brojVrhova = 0;
        FILE *datoteka = fopen(imeDatoteke, "r");
        char znak;
        GLint a, b, c;
        GLfloat x, y, z;
        if (datoteka == NULL)
        {
            printf("Ne mogu otvoriti datoteku\n");
            exit(0);
        }
        znak = fgetc(datoteka);
        while (!feof(datoteka))
        {
            if (znak == '#')
            {
                fscanf(datoteka, "%*[^\n]\n");
                /*printf("Nasao komentar.\n");*/
            }
            else if (znak == 'v')
            {
                fscanf(datoteka, " %f %f %f\n", &x, &y, &z);
                vrhovi.push_back(x);
                vrhovi.push_back(y);
                vrhovi.push_back(z);
                brojVrhova++;
                /*printf("Nasao vrh, %lf, %lf, %lf.\n", x, y, z);*/
            }
            else if (znak == 'f')
            {
                fscanf(datoteka, " %d %d %d\n", &a, &b, &c);
                /*printf("Nasao trokut %d, %d, %d.\n", a, b, c);*/
                trokuti.push_back(--a); /*Stavljam -1 da mi je prvi 0 a ne 1 */
                trokuti.push_back(--b);
                trokuti.push_back(--c);
                brojTrokuta++;
            }
            else
            {
                /*Nasao nelegalan znak na pocetku redka .obj datoteke */
                printf("%c\n", znak);
                fscanf(datoteka, "%*[^\n]\n");
            }
            znak = fgetc(datoteka);
        }
        printf("Pronasao %d vrhova i %d trokuta.\n", brojVrhova, brojTrokuta);
        fclose(datoteka);
    }

    virtual void prepare()
    {
        glGenVertexArrays(1, &vaoId);
        glBindVertexArray(vaoId);

        glGenBuffers(1, &vboIdVrhovi);
        glBindBuffer(GL_ARRAY_BUFFER, vboIdVrhovi);
        glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * vrhovi.size(), &vrhovi[0], GL_STATIC_DRAW);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void *)0);
        glEnableVertexAttribArray(0);

        glGenBuffers(1, &vboIdTrokuti);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboIdTrokuti);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * trokuti.size(), &trokuti[0], GL_STATIC_DRAW);

        glBindVertexArray(0);
    }

    virtual void draw()
    {
        glColor3f(0.5, 0, 0.5);
        glUniform4f(glGetUniformLocation(shaderProgram, "color"), 0.5, 0.5, 0.5, 1);
        glBindVertexArray(vaoId);
        glDrawElements(GL_TRIANGLES, trokuti.size(), GL_UNSIGNED_INT, (void *)0);
        glBindVertexArray(0);
    }

    virtual void clean()
    {
        glDeleteBuffers(1, &vboIdVrhovi);
        glDeleteBuffers(1, &vboIdTrokuti);
        glDeleteVertexArrays(1, &vaoId);
    }

private:
    GLuint vaoId;
    GLuint vboIdVrhovi;
    GLuint vboIdTrokuti;
    GLuint brojVrhova;
    GLuint brojTrokuta;
    std::vector<GLfloat> vrhovi;
    std::vector<GLuint> trokuti;
};

Prozor prozor;
struct Kamera kamera;
RootSceneGraphNode root;

std::vector<struct Cestica> cestice;

GLuint texture;

int main(int argc, char **argv)
{
    prozor.width = 300;
    prozor.height = 300;

    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
    glutInitWindowSize(prozor.width, prozor.height);
    glutInitWindowPosition(100, 100);
    glutInit(&argc, argv);

    prozor.windowID = glutCreateWindow("Drugi labos");

    GLenum err = glewInit();
    if (GLEW_OK != err)
    {
        /* Problem: glewInit failed, something is seriously wrong. */
        fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
    }

    init();

    glutReshapeFunc(myReshape);
    glutDisplayFunc(myDisplay);
    glutKeyboardFunc(myKeyboard);
    glutTimerFunc(16, iduciFrame, 0);
    glutMouseFunc(mouse_func);
    glutMotionFunc(motion_func);
    glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);
    glutMainLoop();
    root.cleanUp();
    std::cout << "Uspjesno izasao\n";
    return 0;
}

#define BROJCESTICA 100

void init()
{

    const char *vertexShaderSource = "#version 330 core\n"
                                     "layout (location = 0) in vec3 aPos;\n"
                                     "void main()\n"
                                     "{\n"
                                     "   gl_Position = gl_ModelViewProjectionMatrix * vec4(aPos.x, aPos.y, aPos.z, 1.0);\n"
                                     "}\0";

    vertexShader = glCreateShader(GL_VERTEX_SHADER);

    glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
    glCompileShader(vertexShader);

    int success;
    char infoLog[512];
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n"
                  << infoLog << std::endl;
    }

    const char *fragmentShaderSource = "#version 330 core\n"
                                       "out vec4 FragColor;\n"
                                       "uniform vec4 color;\n"
                                       "void main()\n"
                                       "{\n"
                                       "FragColor = vec4(color.r, color.g, color.b, color.a);\n"
                                       "}\0";

    fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
    glCompileShader(fragmentShader);

    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n"
                  << infoLog << std::endl;
    }

    shaderProgram = glCreateProgram();

    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);
    glLinkProgram(shaderProgram);

    glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
    if (!success)
    {
        glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::LINK FAILED\n"
                  << infoLog << std::endl;
    }

    glUseProgram(shaderProgram);

    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    //glUseProgram(0);

    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    int width, height, nrChannels;
    unsigned char *data = stbi_load("teksture/snow.bmp", &width, &height, &nrChannels, 0);
    std::cout << "sirina = " << width << " visina = " << height << " kanali = " << nrChannels << "\n";
    if (data)
    {
        if(nrChannels == 3){
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
            glGenerateMipmap(GL_TEXTURE_2D);
        }else if(nrChannels == 4){
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
            glGenerateMipmap(GL_TEXTURE_2D);
        }
    }
    else
    {
        std::cout << "Failed to load texture" << std::endl;
    }
    stbi_image_free(data);

    for (int i = 0; i < BROJCESTICA; i++)
    {
        struct Cestica cestica;
        cestica.pozx = random_double(-10, 10);
        cestica.pozy = random_double(0, 10);
        cestica.pozz = random_double(-10, 10);
        cestica.zivot = random_double(-1, 1);
        cestica.vx = random_double(-0.2, 0.2);
        cestica.vy = random_double(-0.2, 0.2);
        cestica.vz = random_double(-0.2, 0.2);
        cestice.push_back(cestica);
    }

    kamera = {0.0, 5.0, 9.0,
              0.0, 3.0, 5.0,
              0.0, 1.0, 0.0};

    root.addChild(new ModelSceneGraphNode(new Podloga()));
    TranslateSceneGraphNode *translate = new TranslateSceneGraphNode(6, 4, 0);
    RotatedAnimatedSceneGraphNode *rotate = new RotatedAnimatedSceneGraphNode(0, 1, 0, 90, 36);
    ModelSceneGraphNode *model = new ModelSceneGraphNode(new UcitaniModel("modeli/aircraft747.obj"));
    ModelSceneGraphNode *modelGrad = new ModelSceneGraphNode(new UcitaniModel("modeli/ArabianCity.obj"));
    TranslateSceneGraphNode *translatedGrad = new TranslateSceneGraphNode(5, 0.55, -5);
    ScaledSceneGraphNode *scaledGrad = new ScaledSceneGraphNode(10, 10, 10);
    scaledGrad->addChild(modelGrad);
    translatedGrad->addChild(scaledGrad);
    ScaledSceneGraphNode *scaled = new ScaledSceneGraphNode(5, 5, 5);
    scaled->addChild(model);
    translate->addChild(scaled);
    rotate->addChild(translate);
    root.addChild(rotate);
    root.addChild(translatedGrad);
    root.prepare();
}

void drawCestica()
{
    glm::vec3 ociste = glm::vec3(kamera.ocisteX, kamera.ocisteY, kamera.ocisteZ);
    //std::sort(cestice.begin(), cestice.end(), [](struct Cestica c1, struct Cestica c2) -> bool {
    //    return glm::length(glm::vec3(c1.pozx - kamera.ocisteX, c1.pozy - kamera.ocisteX, c1.pozz - kamera.ocisteZ)) > glm::length(glm::vec3(c2.pozx - kamera.ocisteX, c2.pozy - kamera.ocisteX, c2.pozz - kamera.ocisteZ));
    //});
    glDepthMask(GL_FALSE);

    for (auto cestica : cestice)
    {
        glm::vec3 pozicija = glm::vec3(cestica.pozx, cestica.pozy, cestica.pozz);
        glColor4f(1, 1, 1, 1 - fabs(cestica.zivot));
        glUniform4f(glGetUniformLocation(shaderProgram, "color"), 1, 1, 1, 1 - fabs(cestica.zivot));
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glTranslatef(cestica.pozx, cestica.pozy, cestica.pozz);
        glm::vec3 s = glm::vec3(0, 1, 0);
        glm::vec3 e = ociste - pozicija;
        glm::vec3 os = glm::cross(s, e);
        double cosfi = glm::dot(s, e) / glm::length(e);
        double fi = radToDeg(acos(cosfi));
        glRotatef(fi, os.x, os.y, os.z);
        glEnable(GL_TEXTURE_2D);
        glBegin(GL_TRIANGLE_FAN);
        glTexCoord2f(1, 1);
        glVertex3f(1 - fabs(cestica.zivot), 0, 1 - fabs(cestica.zivot));
        glTexCoord2f(1, 0);
        glVertex3f(1 - fabs(cestica.zivot), 0, -(1 - fabs(cestica.zivot)));
        glTexCoord2f(0, 0);
        glVertex3f(-(1 - fabs(cestica.zivot)), 0, -(1 - fabs(cestica.zivot)));
        glTexCoord2f(0, 1);
        glVertex3f(-(1 - fabs(cestica.zivot)), 0, 1 - fabs(cestica.zivot));
        glEnd();
        glDisable(GL_TEXTURE_2D);
        glPopMatrix();
    }
    glDepthMask(GL_TRUE);
}

void updateCestica(double dt)
{
    for (auto iter = cestice.begin(); iter != cestice.end(); ++iter)
    {
        if ((*iter).zivot < -1)
        {
            (*iter).pozx = random_double(-10, 10);
            (*iter).pozy = random_double(0, 10);
            (*iter).pozz = random_double(-10, 10);
            (*iter).zivot = 1;
            (*iter).vx = random_double(-0.2, 0.2);
            (*iter).vy = random_double(-0.2, 0.2);
            (*iter).vz = random_double(-0.2, 0.2);
        }
        (*iter).pozx += (*iter).vx * dt;
        (*iter).pozy += (*iter).vy * dt;
        (*iter).pozz += (*iter).vz * dt;
        (*iter).zivot -= dt * 0.1;
    }
}

void myDisplay(void)
{
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(kamera.ocisteX, kamera.ocisteY, kamera.ocisteZ,
              kamera.gledisteX, kamera.gledisteY, kamera.gledisteZ,
              kamera.vUpX, kamera.vUpY, kamera.vUpZ);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    glClearColor((float)135 / 255, (float)206 / 255, (float)235 / 255, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glPointSize(1.0);
    glColor3f(0.0f, 0.0f, 0.0f);
    crtaj();
    glutSwapBuffers();
}

void myReshape(int w, int h)
{
    prozor.width = w;
    prozor.height = h;
    glViewport(0, 0, prozor.width, prozor.height);
    //glClearColor(1,1,1,0);
    glClearColor((float)135 / 255, (float)206 / 255, (float)235 / 255, 0.0f); // boja pozadine
    //glClearColor(0,0,0,0); //crna pozadina
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glPointSize(1.0);
    glColor3f(0.0f, 0.0f, 0.0f);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0, (float)prozor.width / prozor.height, 0.5, 1000.0);
    // kut pogleda, x/y, prednja i straznja ravnina odsjecanja
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(kamera.ocisteX, kamera.ocisteY, kamera.ocisteZ,
              kamera.gledisteX, kamera.gledisteY, kamera.gledisteZ,
              kamera.vUpX, kamera.vUpY, kamera.vUpZ);
    // ociste x,y,z
    // glediste x,y,z
    // up vektor x,y,z
}

void myKeyboard(unsigned char theKey, int mouseX, int mouseY)
{
    //dodati
}

void crtaj()
{
    static int inicijaliziraniSat;
    static std::chrono::_V2::system_clock::time_point t1;
    static std::chrono::_V2::system_clock::time_point t2;
    if (!inicijaliziraniSat)
    {
        inicijaliziraniSat = 1;
        t1 = std::chrono::high_resolution_clock::now();
    }
    t2 = t1;
    t1 = std::chrono::high_resolution_clock::now();
    double trajanje = static_cast<double>(std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t2).count()) / 1000.0;

    root.update(trajanje);
    glUseProgram(shaderProgram);
    root.draw();
    glUseProgram(0);
    updateCestica(trajanje);
    drawCestica();
    glBegin(GL_LINES);
    glColor3f(1, 0, 0);
    glVertex3f(kamera.gledisteX, kamera.gledisteY, kamera.gledisteZ);
    glVertex3f(kamera.gledisteX + 1, kamera.gledisteY, kamera.gledisteZ);
    glColor3f(0, 1, 0);
    glVertex3f(kamera.gledisteX, kamera.gledisteY, kamera.gledisteZ);
    glVertex3f(kamera.gledisteX, kamera.gledisteY + 1, kamera.gledisteZ);
    glColor3f(0, 0, 1);
    glVertex3f(kamera.gledisteX, kamera.gledisteY, kamera.gledisteZ);
    glVertex3f(kamera.gledisteX, kamera.gledisteY, kamera.gledisteZ + 1);
    glEnd();
}

/*
void ucitajScenu(const char *imeDatotekeScene)
{
    FILE *datoteka = fopen(imeDatotekeScene, "r");
    char znak;
    GLint a, b, c;
    GLdouble x, y, z;
    if (datoteka == NULL)
    {
        printf("Ne mogu otvoritit datoteku\n");
        exit(0);
    }
    znak = fgetc(datoteka);
    while (!feof(datoteka))
    {
        if (znak == '#')
        {
            fscanf(datoteka, "%*[^\n]\n");
            //printf("Nasao komentar.\n");
        }
        else if (znak == 'v')
        {
            fscanf(datoteka, " %lf %lf %lf\n", &x, &y, &z);
            tockeX.push_back(x);
            tockeY.push_back(y);
            tockeZ.push_back(z);
            brojVrhova++;
            //printf("Nasao vrh, %lf, %lf, %lf.\n", x, y, z);
        }
        else if (znak == 'f')
        {
            fscanf(datoteka, " %d %d %d\n", &a, &b, &c);
            //printf("Nasao trokut %d, %d, %d.\n", a, b, c);
            prviVrh.push_back(--a); //Stavljam -1 da mi je prvi 0 a ne 1
            drugiVrh.push_back(--b);
            treciVrh.push_back(--c);
            brojTrokuta++;
        }
        else
        {
            //Nasao nelegalan znak na pocetku redka .obj datoteke
            printf("%c\n", znak);
            fscanf(datoteka, "%*[^\n]\n");
        }
        znak = fgetc(datoteka);
    }
    printf("Pronasao %d vrhova i %d trokuta.\n", brojVrhova, brojTrokuta);
    fclose(datoteka);
}*/

int old_x = 0;
int old_y = 0;
int valid = 0;
int mode = 0;

void mouse_func(int button, int state, int x, int y)
{
    old_x = x;
    old_y = y;
    if (button == GLUT_LEFT_BUTTON)
    {
        mode = 1;
        valid = state == GLUT_DOWN;
    }
    else if (button == GLUT_RIGHT_BUTTON)
    {
        mode = -1;
        valid = state == GLUT_DOWN;
    }
    else if (button == 3 && state == GLUT_DOWN)
    {
        //std::cout << "kotac gore\n";
        zoom(1);
    }
    else if (button == 4 && state == GLUT_DOWN)
    {
        //std::cout << "kotac dolje\n";
        zoom(-1);
    }
}

void zoom(int dir)
{
    glm::vec3 pomak = glm::vec3(kamera.gledisteX - kamera.ocisteX, kamera.gledisteY - kamera.ocisteY, kamera.gledisteZ - kamera.ocisteZ);
    glm::vec3 ociste = glm::vec3(kamera.ocisteX, kamera.ocisteY, kamera.ocisteZ);
    glm::vec3 glediste = glm::vec3(kamera.gledisteX, kamera.gledisteY, kamera.gledisteZ);
    if (dir == 1)
    {
        glediste = glediste + pomak;
        ociste = ociste + pomak;
    }
    else if (dir == -1)
    {
        glediste = glediste - pomak;
        ociste = ociste - pomak;
    }

    kamera.gledisteX = glediste.x;
    kamera.gledisteY = glediste.y;
    kamera.gledisteZ = glediste.z;
    kamera.ocisteX = ociste.x;
    kamera.ocisteY = ociste.y;
    kamera.ocisteZ = ociste.z;
    glutPostRedisplay();
}

void motion_func(int x, int y)
{
    if (valid)
    {
        int dx = old_x - x;
        int dy = old_y - y;
        updatePerspective(dx, dy);
    }
    old_x = x;
    old_y = y;
}

void updatePerspective(int dx, int dy)
{
    if (mode == 1)
    {

        glm::vec3 vup = glm::vec3(0, 1, 0);
        glm::vec3 vektor = glm::vec3(kamera.gledisteX - kamera.ocisteX, kamera.gledisteY - kamera.ocisteY, kamera.gledisteZ - kamera.ocisteZ);
        glm::vec3 rezultat = glm::vec3(0);
        rezultat += vektor * (float)cos(degToRad(dx));
        rezultat += glm::cross(vektor, vup) * (float)sin(degToRad(dx));
        rezultat += vup * vektor * vup * (float)(1 - cos(degToRad(dx)));

        kamera.gledisteX = kamera.ocisteX + rezultat.x;
        kamera.gledisteY = kamera.ocisteY + rezultat.y;
        kamera.gledisteZ = kamera.ocisteZ + rezultat.z;

        vup = glm::vec3(1, 0, 0);
        vektor = glm::vec3(kamera.gledisteX - kamera.ocisteX, kamera.gledisteY - kamera.ocisteY, kamera.gledisteZ - kamera.ocisteZ);
        rezultat = glm::vec3(0);
        rezultat += vektor * (float)cos(degToRad(dy));
        rezultat += glm::cross(vektor, vup) * (float)sin(degToRad(dy));
        rezultat += vup * vektor * vup * (float)(1 - cos(degToRad(dy)));

        kamera.gledisteX = kamera.ocisteX + rezultat.x;
        kamera.gledisteY = kamera.ocisteY + rezultat.y;
        kamera.gledisteZ = kamera.ocisteZ + rezultat.z;
    }
    else if (mode == -1)
    {
        glm::vec3 vup = glm::vec3(0, 1, 0);
        glm::vec3 vektor = glm::vec3(kamera.ocisteX - kamera.gledisteX, kamera.ocisteY - kamera.gledisteY, kamera.ocisteZ - kamera.gledisteZ);
        glm::vec3 rezultat = glm::vec3(0);
        rezultat += vektor * (float)cos(degToRad(dx));
        rezultat += glm::cross(vektor, vup) * (float)sin(degToRad(dx));
        rezultat += vup * vektor * vup * (float)(1 - cos(degToRad(dx)));

        kamera.ocisteX = kamera.gledisteX + rezultat.x;
        kamera.ocisteY = kamera.gledisteY + rezultat.y;
        kamera.ocisteZ = kamera.gledisteZ + rezultat.z;

        vup = glm::vec3(1, 0, 0);
        vektor = glm::vec3(kamera.ocisteX - kamera.gledisteX, kamera.ocisteY - kamera.gledisteY, kamera.ocisteZ - kamera.gledisteZ);
        rezultat = glm::vec3(0);
        rezultat += vektor * (float)cos(degToRad(dy));
        rezultat += glm::cross(vektor, vup) * (float)sin(degToRad(dy));
        rezultat += vup * vektor * vup * (float)(1 - cos(degToRad(dy)));

        kamera.ocisteX = kamera.gledisteX + rezultat.x;
        kamera.ocisteY = kamera.gledisteY + rezultat.y;
        kamera.ocisteZ = kamera.gledisteZ + rezultat.z;
    }
    glutPostRedisplay();
}

void iduciFrame(int broj)
{
    if (glutGetWindow() == 0)
    {
        return;
    }

    double msDoIduceg = static_cast<double>(1000) / 60;

    glutPostRedisplay();
    glutTimerFunc(msDoIduceg, iduciFrame, 0);
}
