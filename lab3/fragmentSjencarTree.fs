#version 330 core

out vec4 FragColor;
uniform vec4 color;
uniform vec3 pozicijaSunca;
in vec3 outPos;
in vec3 normala;

const vec3 boja_sunca = vec3(0.933, 0.909, 0.349);

void main()
{
    //stabla imaju obrnuti poredak vrhova
    float k = -5.0 * dot(normala, normalize(pozicijaSunca - outPos));
    FragColor = vec4(0.4 * vec3(color.r * (3.0 - 0.2 * outPos.y), color.g * outPos.y * 0.2, color.b), color.a);
    vec3 difuzna_komponenta = clamp((boja_sunca) * k,0.0, 1.0);

    FragColor.rgb += difuzna_komponenta;
    FragColor.a = 1.0;

}
