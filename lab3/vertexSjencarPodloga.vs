#version 330 core

layout (location = 0) in vec3 vertexPos;
out vec3 outPos;

uniform mat4 modelMatrica;
uniform mat4 viewMatrica;
uniform mat4 projekcijskaMatrica;
uniform bool clip;
uniform vec4 clip_ravnina;

void main()
{
   if(clip){
      gl_ClipDistance[0] = dot(modelMatrica * vec4(vertexPos, 1.0), clip_ravnina);
   }
   gl_Position = projekcijskaMatrica * viewMatrica * modelMatrica * vec4(vertexPos.x, vertexPos.y, vertexPos.z, 1.0);
   outPos = vertexPos;
}
