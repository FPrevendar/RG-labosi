#version 330 core

out vec4 FragColor;
in vec4 pozicija_na_ekranu;
in vec2 uvCoords;
in vec3 outPos;

uniform sampler2D refrakcija;
uniform sampler2D refleksija;
uniform sampler2D dudvmap;
uniform sampler2D dubinskaMapa;
uniform sampler2D mapaNormala;

uniform vec3 pozicijaSunca;
uniform float vrijeme;
uniform vec3 pozicija_kamere;

const vec3 boja_sunca = vec3(0.933, 0.909, 0.349);
const float faktor_poplocenja = 6.0;
const float snaga_distorzije = 0.02;
const float brzina_valica = 0.05;
const float specular_alfa = 3.0;
const float specular_k = 1.0;

void main()
{
    float pomak = vrijeme * brzina_valica;
    vec2 koordinate_ekrana = (pozicija_na_ekranu.xy/pozicija_na_ekranu.w)/2.0 + 0.5;
    vec2 refrakcijaKoordinate = vec2(koordinate_ekrana.xy);
    vec2 refleksijaKooridnate = vec2(koordinate_ekrana.x, -koordinate_ekrana.y);

    const float far = 1000;
    const float near = 0.1;
    float dubina = texture(dubinskaMapa, refrakcijaKoordinate).r;
    float dno_vode = 2.0 * near * far / (far + near - (2.0 * dubina - 1.0) * (far - near));
    dubina = gl_FragCoord.z;
    float povrsina_vode = 2.0 * near * far / (far + near - (2.0 * dubina - 1.0) * (far - near));
    float dubinaVode = dno_vode - povrsina_vode;

    vec2 distorzija = (texture(dudvmap, uvCoords * faktor_poplocenja + vec2(sin(pomak), cos(pomak))).rg * 2.0 - 1.0) * snaga_distorzije;

    vec3 normala = normalize(texture(mapaNormala, distorzija).rbg);
    vec3 prema_kameri = normalize(pozicija_kamere - outPos);
    vec3 prema_suncu =  normalize(pozicijaSunca - outPos);
    vec3 reflektirana_zraka = 2 * dot(prema_suncu, normala) * normala - prema_suncu;
    vec3 specular_komponenta = specular_k * pow(clamp(dot(reflektirana_zraka, prema_kameri), 0.0, 1.0), specular_alfa) * boja_sunca;

    refrakcijaKoordinate = clamp(refrakcijaKoordinate + distorzija, 0.001, 0.999);
    refleksijaKooridnate.x = clamp(refleksijaKooridnate.x + distorzija.x, 0.001, 0.999);
    refleksijaKooridnate.y = clamp(refleksijaKooridnate.y + distorzija.y, -0.999, 0.001);

    vec4 bojaRefrakcije = texture(refrakcija, refrakcijaKoordinate);
    vec4 bojaRefleksije = texture(refleksija, refleksijaKooridnate);

    float refleksivnost = dot(prema_kameri,vec3(0.0, 1.0, 0.0));
    refleksivnost = pow(refleksivnost, 2.0);

    FragColor = mix(bojaRefleksije, bojaRefrakcije, refleksivnost);
    FragColor = mix(FragColor, vec4(0.0, 0.2, 0.7, 1.0), 0.2);
    FragColor.a = clamp(10.0 * dubinaVode, 0.0, 1.0);
    FragColor.rgb += specular_komponenta;


}
