#include <stdio.h>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <vector>
#include <iostream>
#include <chrono>
#include <random>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <string>
#include <map>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#define MATH_PI 3.14159265358979323846264338327950288
#define degToRad(kutUStupnjevima) ((kutUStupnjevima)*MATH_PI / 180.0)
#define radToDeg(kutURadijanima) ((kutURadijanima)*180.0 / MATH_PI)

std::vector<GLfloat> razdijeliPodlogu(std::vector<GLfloat> *original);

inline double random_double()
{
    static std::uniform_real_distribution<double> distribution(0.0, 1.0);
    static std::mt19937 generator;
    return distribution(generator);
}

inline double random_double(double min, double max)
{
    return min + (max - min) * random_double();
}


glm::mat4 modelMatrica = glm::mat4(1.0f);
glm::mat4 viewMatrica = glm::mat4(1.0f);
glm::mat4 projekcijskaMatrica = glm::mat4(1.0f);

void myDisplay();
void myReshape(int width, int height);
void myKeyboard(unsigned char theKey, int mouseX, int mouseY);
void iduciFrame(int);
void init();
void mouse_func(int button, int state, int x, int y);
void motion_func(int x, int y);
void updatePerspective(int, int);
void crtaj();
void zoom(int);
void drawCestica();
void updateCestica(double);
void initVodaBufferi();
void initCestica();

constexpr float visinaVode = 2.0;

constexpr int sirina_refleksije = 1080;
constexpr int visina_refleksije = 1920;
constexpr int sirina_refrakcije = 1920;
constexpr int visina_refrakcije = 1080;

GLuint reflekcijaFrameBuffer;
GLuint refleksijaTekstura;
GLuint refleksijaDepthTekstura;

GLuint refrakcijaFrameBuffer;
GLuint refrakcijaTekstura;
GLuint refrakcijaDepthTekstura;

glm::vec4 clip_ravnine[] = {glm::vec4(0, -1, 0, visinaVode + 0.1), glm::vec4(0, 1, 0, -visinaVode - 0.1)};

GLuint dudvmapId;
GLuint mapaNormalaId;

glm::vec3 pozicija_sunca = glm::vec3(-40, 30, -40);

int clip_ravnina = -1;

const char* vertexSjencarVodaPut = "vertexSjencarVoda.vs";
const char* fragmentSjencarVodaPut = "fragmentSjencarVoda.fs";

const char* vertexSjencarModelPut = "vertexSjencarModel.vs";
const char* fragmentSjencarModelPut = "fragmentSjencarModel.fs";

const char* vertexSjencarTreePut = "vertexSjencarTree.vs";
const char* fragmentSjencarTreePut = "fragmentSjencarTree.fs";

const char* vertexSjencarPodlogaPut = "vertexSjencarPodloga.vs";
const char* fragmentSjencarPodlogaPut = "fragmentSjencarPodloga.fs";


struct Kamera
{
    glm::vec3 ociste;

    glm::vec3 glediste;

    glm::vec3 vup;
};

struct Cestica
{
    GLfloat pozx;
    GLfloat pozy;
    GLfloat pozz;

    GLfloat vx;
    GLfloat vy;
    GLfloat vz;

    GLfloat zivot;
};

class Prozor
{
public:
    GLuint windowID;
    GLuint width;
    GLuint height;
};

Prozor prozor;
struct Kamera kamera;

class Sjencar
{
public:
    GLuint programID;

    Sjencar(const char *putDoVertexa, const char *putDoFragmenta);
    void koristi();
    void setBool(const char* name, bool value){
        glUniform1i(glGetUniformLocation(programID, name), (int)value);
    }
    void setInt(const char* name, int value){
        glUniform1i(glGetUniformLocation(programID, name), value);
    }
    void setFloat(const char *name, float value){
        glUniform1f(glGetUniformLocation(programID, name), value);
    }
    void setVec2(const char *name, const glm::vec2 &value){
        glUniform2fv(glGetUniformLocation(programID, name), 1, glm::value_ptr(value));
    }
    void setVec2(const char *name, float x, float y){
        glUniform2f(glGetUniformLocation(programID, name), x, y);
    }
    void setVec3(const char *name, const glm::vec3 &value){
        glUniform3fv(glGetUniformLocation(programID, name), 1, glm::value_ptr(value));
    }
    void setVec3(const char *name, float x, float y, float z){
        glUniform3f(glGetUniformLocation(programID, name), x, y, z);
    }
    void setVec4(const char *name, const glm::vec4 &value){
        glUniform4fv(glGetUniformLocation(programID, name), 1, glm::value_ptr(value));
    }
    void setVec4(const char *name, float x, float y, float z, float w){
        glUniform4f(glGetUniformLocation(programID, name), x, y, z, w);
    }
    void setMat2(const char *name, const glm::mat2 &mat){
        glUniformMatrix2fv(glGetUniformLocation(programID, name), 1, GL_FALSE, glm::value_ptr(mat));
    }
    void setMat3(const char *name, const glm::mat3 &mat){
        glUniformMatrix3fv(glGetUniformLocation(programID, name), 1, GL_FALSE, glm::value_ptr(mat));
    }
    void setMat4(const char *name, const glm::mat4 &mat){
        glUniformMatrix4fv(glGetUniformLocation(programID, name), 1, GL_FALSE, glm::value_ptr(mat));
    }
};

Sjencar::Sjencar(const char *putDoVertexa, const char *putDoFragmenta)
{
    std::string vertexKodString;
    std::string fragmentKodString;
    std::ifstream vShaderFile;
    std::ifstream fShaderFile;
    vShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    fShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    try
    {
        vShaderFile.open(putDoVertexa);
        fShaderFile.open(putDoFragmenta);
        std::stringstream vShaderStream, fShaderStream;
        vShaderStream << vShaderFile.rdbuf();
        fShaderStream << fShaderFile.rdbuf();
        vShaderFile.close();
        fShaderFile.close();
        vertexKodString = vShaderStream.str();
        fragmentKodString = fShaderStream.str();
    }
    catch (std::ifstream::failure &e)
    {
        std::cout << "Ne mogu procitati datoteke sjencara\n";
    }
    const char *vertexSjencarKod = vertexKodString.c_str();
    const char *fragmentSjencarKod = fragmentKodString.c_str();

    GLuint vertexID, fragmentID;
    int uspjeh;
    char log[512];

    vertexID = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexID, 1, &vertexSjencarKod, NULL);
    glCompileShader(vertexID);
    glGetShaderiv(vertexID, GL_COMPILE_STATUS, &uspjeh);
    if (!uspjeh)
    {
        glGetShaderInfoLog(vertexID, 512, NULL, log);
        std::cout << "Neuspjsno prevodjenje vertex sjencara\n";
        std::cout << log << "\n";
    }
    fragmentID = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentID, 1, &fragmentSjencarKod, NULL);
    glCompileShader(fragmentID);
    glGetShaderiv(fragmentID, GL_COMPILE_STATUS, &uspjeh);
    if (!uspjeh)
    {
        glGetShaderInfoLog(fragmentID, 512, NULL, log);
        std::cout << "Neuspjsno prevodjenje fragment sjencara\n";
        std::cout << log << "\n";
    }
    programID = glCreateProgram();
    glAttachShader(programID, vertexID);
    glAttachShader(programID, fragmentID);
    glLinkProgram(programID);
    glGetProgramiv(programID, GL_LINK_STATUS, &uspjeh);
    if (!uspjeh)
    {
        glGetProgramInfoLog(programID, 512, NULL, log);
        std::cout << "Neuspjesno povezivanje programa\n"
                  << log << "\n";
    }
    glDeleteShader(vertexID);
    glDeleteShader(fragmentID);
}

void Sjencar::koristi()
{
    glUseProgram(this->programID);
}

class Model
{
public:
    virtual void prepare() = 0;
    virtual void draw() = 0;
    virtual void clean() = 0;
};

class SceneGraphNode
{
public:
    virtual void prepare() = 0;
    virtual void update(double t) = 0;
    virtual void draw() = 0;
    virtual void addChild(SceneGraphNode *child) = 0;
    virtual void cleanUp() = 0;
};

class AbstractSceneGraphNode : public SceneGraphNode
{
protected:
    std::vector<SceneGraphNode *> children = std::vector<SceneGraphNode *>();

public:
    virtual void prepare()
    {
        for (auto child : children)
        {
            child->prepare();
        }
    };
    virtual void update(double t)
    {
        for (auto child : children)
        {
            child->update(t);
        }
    }
    virtual void draw()
    {
        for (auto child : children)
        {
            child->draw();
        }
    }
    virtual void addChild(SceneGraphNode *child)
    {
        children.push_back(child);
    }

    virtual void cleanUp()
    {
        for (auto child : children)
        {
            child->cleanUp();
        }
    }
};

class RootSceneGraphNode : public AbstractSceneGraphNode
{
    //nista posebno
};

class ModelSceneGraphNode : public AbstractSceneGraphNode
{
public:
    ModelSceneGraphNode(Model *m)
    {
        this->model = m;
    }
    virtual void prepare()
    {
        model->prepare();
    }
    virtual void update(double t){};

    virtual void draw()
    {
        model->draw();
    }
    virtual void addChild(SceneGraphNode *child) {}

    virtual void cleanUp()
    {
        model->clean();
    }

private:
    Model *model;
};

class TranslateSceneGraphNode : public AbstractSceneGraphNode
{
public:
    TranslateSceneGraphNode(GLfloat x, GLfloat y, GLfloat z)
    {
        this->x = x;
        this->y = y;
        this->z = z;
    }

    virtual void draw()
    {
        glm::mat4 spremljeni = modelMatrica;
        modelMatrica = glm::translate(modelMatrica, glm::vec3(x,y,z));
        /*
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glTranslatef(x, y, z);
        */
        for (auto child : children)
        {
            child->draw();
        }
        /*
        glPopMatrix();
        */
        modelMatrica = spremljeni;
    }

private:
    GLfloat x, y, z;
};

class RotatedSceneGraphNode : public AbstractSceneGraphNode
{
public:
    RotatedSceneGraphNode(GLfloat x, GLfloat y, GLfloat z, GLfloat fi)
    {
        this->x = x;
        this->y = y;
        this->z = z;
        this->fi = fi;
    }

    virtual void draw()
    {
        glm::mat4 spremljeni = modelMatrica;
        modelMatrica = glm::rotate(modelMatrica, glm::radians(fi), glm::vec3(x,y,z));
        /*
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glRotatef(fi, x, y, z);
        */
        for (auto child : children)
        {
            child->draw();
        }
        //glPopMatrix();
        modelMatrica = spremljeni;
    }

private:
    GLfloat x, y, z, fi;
};

class RotatedAnimatedSceneGraphNode : public AbstractSceneGraphNode
{
public:
    RotatedAnimatedSceneGraphNode(GLfloat x, GLfloat y, GLfloat z, GLfloat fi, GLfloat omega)
    {
        this->x = x;
        this->y = y;
        this->z = z;
        this->fi = fi;
        this->omega = omega;
    }

    virtual void draw()
    {
        glm::mat4 spremljeni = modelMatrica;
        modelMatrica = glm::rotate(modelMatrica, glm::radians(fi), glm::vec3(x,y,z));
        /*
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glRotatef(fi, x, y, z);
        */
        for (auto child : children)
        {
            child->draw();
        }
        //glPopMatrix();
        modelMatrica = spremljeni;
    }

    virtual void update(double t)
    {
        fi -= t * omega;
        if (fi > 360)
        {
            fi = 0;
        }
        for (auto child : children)
        {
            child->update(t);
        }
    }

private:
    GLfloat x, y, z, fi, omega;
};

class ScaledSceneGraphNode : public AbstractSceneGraphNode
{
public:
    ScaledSceneGraphNode(GLfloat x, GLfloat y, GLfloat z)
    {
        this->x = x;
        this->y = y;
        this->z = z;
    }

    virtual void draw()
    {
        glm::mat4 spremljeni = modelMatrica;
        modelMatrica = glm::scale(modelMatrica, glm::vec3(x,y,z));
        /*
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glScalef(x, y, z);
        */
        for (auto child : children)
        {
            child->draw();
        }
        //glPopMatrix();
        modelMatrica = spremljeni;
    }

private:
    GLfloat x, y, z;
};

class Podloga : public Model
{
public:
    virtual void prepare()
    {
        sjencar = new Sjencar(vertexSjencarPodlogaPut, fragmentSjencarPodlogaPut);
        vrhovi = razdijeliPodlogu(&vrhovi);
        vrhovi = razdijeliPodlogu(&vrhovi);
        vrhovi = razdijeliPodlogu(&vrhovi);
        vrhovi = razdijeliPodlogu(&vrhovi);
        vrhovi = razdijeliPodlogu(&vrhovi);
        glGenVertexArrays(1, &vaoId);
        glBindVertexArray(vaoId);
        glGenBuffers(1, &vboId);
        glBindBuffer(GL_ARRAY_BUFFER, vboId);
        glBufferData(GL_ARRAY_BUFFER, vrhovi.size() * sizeof(GLfloat), &vrhovi[0], GL_STATIC_DRAW);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void *)0);
        glEnableVertexAttribArray(0);
        glBindVertexArray(0);
    }

    virtual void draw()
    {

        sjencar->koristi();
        sjencar->setVec4("boja", glm::vec4(0,0,1,0.5));
        sjencar->setMat4("projekcijskaMatrica", projekcijskaMatrica);
        sjencar->setMat4("viewMatrica", viewMatrica);
        sjencar->setMat4("modelMatrica", modelMatrica);
        sjencar->setVec4("color", (float)77/255, (float)111/255, (float)57/255, 1);
        if(clip_ravnina == -1){
            sjencar->setBool("clip", false);
        }else{
            sjencar->setBool("clip", true);
            sjencar->setVec4("clip_ravnina", clip_ravnine[clip_ravnina]);
        }
        glBindVertexArray(vaoId);
        glDrawArrays(GL_TRIANGLES, 0, vrhovi.size() / 3);
        glBindVertexArray(0);
    }

    virtual void clean()
    {
        glDeleteBuffers(1, &vboId);
        glDeleteVertexArrays(1, &vaoId);
    }

private:
    GLuint vaoId;
    GLuint vboId;
    Sjencar *sjencar;
    std::vector<GLfloat> vrhovi = {
        20.0, 0.0, 20.0,
        20.0, 0.0, -20.0,
        -20.0, 0.0, -20.0,
        -20.0, 0, -20.0,
        -20.0, 0.0, 20.0,
        20.0, 0.0, 20.0,
    };
};

class UcitaniModel : public Model
{
public:
    UcitaniModel(){};
    UcitaniModel(const char *imeDatoteke)
    {
        sjencar = new Sjencar(vertexSjencarModelPut, fragmentSjencarModelPut);
        ucitaj(imeDatoteke);
        racunajNormale();
        std::cout << "izracunao je " << this->normaleVrhova.size()/3 << "\n";
        spojiVrhoveINormale();
    }

    UcitaniModel(const char *imeDatoteke, const char *vshader, const char *fshader){
        sjencar = new Sjencar(vshader, fshader);
        ucitaj(imeDatoteke);
        racunajNormale();
        std::cout << "izracunao je " << this->normaleVrhova.size()/3 << "\n";
        spojiVrhoveINormale();
    }

    virtual void prepare()
    {
        glGenVertexArrays(1, &vaoId);
        glBindVertexArray(vaoId);

        glGenBuffers(1, &vboIdVrhovi);
        glBindBuffer(GL_ARRAY_BUFFER, vboIdVrhovi);
        glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * spojeniVrhoviINormale.size(), &spojeniVrhoviINormale[0], GL_STATIC_DRAW);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void *)0);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);

        glGenBuffers(1, &vboIdTrokuti);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboIdTrokuti);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * trokuti.size(), &trokuti[0], GL_STATIC_DRAW);

        glBindVertexArray(0);
    }

    virtual void draw()
    {
        sjencar->koristi();
        sjencar->setVec4("color", 0.4, 0.8, 0.2, 1);
        sjencar->setMat4("projekcijskaMatrica", projekcijskaMatrica);
        sjencar->setMat4("viewMatrica", viewMatrica);
        sjencar->setMat4("modelMatrica", modelMatrica);
        sjencar->setMat3("matricaZaNormale", glm::transpose(glm::inverse(glm::mat3(modelMatrica))));
        sjencar->setVec3("pozicijaSunca", pozicija_sunca);
        if(clip_ravnina == -1){
            sjencar->setBool("clip", false);
        }else{
            sjencar->setBool("clip", true);
            sjencar->setVec4("clip_ravnina", clip_ravnine[clip_ravnina]);
        }
        glBindVertexArray(vaoId);
        glDrawElements(GL_TRIANGLES, trokuti.size(), GL_UNSIGNED_INT, (void *)0);
        glBindVertexArray(0);
    }

    virtual void clean()
    {
        glDeleteBuffers(1, &vboIdVrhovi);
        glDeleteBuffers(1, &vboIdTrokuti);
        glDeleteVertexArrays(1, &vaoId);
    }

private:
    GLuint vaoId;
    GLuint vboIdVrhovi;
    GLuint vboIdTrokuti;
    GLuint brojVrhova;
    GLuint brojTrokuta;
    std::vector<GLfloat> vrhovi;
    std::vector<GLuint> trokuti;
    std::vector<GLfloat> normaleVrhova;
    std::vector<GLfloat> spojeniVrhoviINormale;
    Sjencar *sjencar;

    void ucitaj(const char* imeDatoteke){
        std::cout << "Ucitavam " << imeDatoteke << "\n";
        vrhovi = std::vector<GLfloat>();
        trokuti = std::vector<GLuint>();
        brojTrokuta = 0;
        brojVrhova = 0;
        FILE *datoteka = fopen(imeDatoteke, "r");
        char znak;
        GLint a, b, c;
        GLfloat x, y, z;
        if (datoteka == NULL)
        {
            printf("Ne mogu otvoriti datoteku\n");
            exit(0);
        }
        znak = fgetc(datoteka);
        while (!feof(datoteka))
        {
            if (znak == '#')
            {
                fscanf(datoteka, "%*[^\n]\n");
                /*printf("Nasao komentar.\n");*/
            }
            else if (znak == 'v')
            {
                fscanf(datoteka, " %f %f %f\n", &x, &y, &z);
                vrhovi.push_back(x);
                vrhovi.push_back(y);
                vrhovi.push_back(z);
                brojVrhova++;
                /*printf("Nasao vrh, %lf, %lf, %lf.\n", x, y, z);*/
            }
            else if (znak == 'f')
            {
                fscanf(datoteka, " %d %d %d\n", &a, &b, &c);
                /*printf("Nasao trokut %d, %d, %d.\n", a, b, c);*/
                trokuti.push_back(c - 1); /*Stavljam -1 da mi je prvi 0 a ne 1 */
                trokuti.push_back(b - 1);
                trokuti.push_back(a - 1);
                brojTrokuta++;
            }
            else
            {
                /*Nasao nelegalan znak na pocetku redka .obj datoteke */
                printf("%c\n", znak);
                fscanf(datoteka, "%*[^\n]\n");
            }
            znak = fgetc(datoteka);
        }
        printf("Pronasao %d vrhova i %d trokuta.\n", brojVrhova, brojTrokuta);
        fclose(datoteka);
    }

     void racunajNormale(){
        int i;
        std::vector<GLfloat> (normaleTrokuta);
        std::map<int,std::vector<int>> (listaSusjednihVrhova);

        for(i = 0; i < brojTrokuta; i++){
            GLuint vrh1 = trokuti[3 * i];
            GLuint vrh2 = trokuti[3 * i + 1];
            GLuint vrh3 = trokuti[3 * i + 2];
            glm::vec3 v1 = glm::vec3(vrhovi[3 * vrh1], vrhovi[3 * vrh1 + 1], vrhovi[3 * vrh1 + 2]);
            glm::vec3 v2 = glm::vec3(vrhovi[3 * vrh2], vrhovi[3 * vrh2 + 1], vrhovi[3 * vrh2 + 2]);
            glm::vec3 v3 = glm::vec3(vrhovi[3 * vrh3], vrhovi[3 * vrh3 + 1], vrhovi[3 * vrh3 + 2]);

            glm::vec3 A = v2 - v1;
            glm::vec3 B = v3 - v1;
            glm::vec3 N = glm::cross(A,B);
            normaleTrokuta.push_back(N.x);
            normaleTrokuta.push_back(N.y);
            normaleTrokuta.push_back(N.z);

            if(listaSusjednihVrhova.find(vrh1) == listaSusjednihVrhova.end()){
                listaSusjednihVrhova.insert(std::pair<int, std::vector<int>>(vrh1, std::vector<int>()));
                listaSusjednihVrhova[vrh1].push_back(i);
            }else{
                listaSusjednihVrhova[vrh1].push_back(i);
            }

            if(listaSusjednihVrhova.find(vrh2) == listaSusjednihVrhova.end()){
                listaSusjednihVrhova.insert(std::pair<int, std::vector<int>>(vrh2, std::vector<int>()));
                listaSusjednihVrhova[vrh2].push_back(i);
            }else{
                listaSusjednihVrhova[vrh2].push_back(i);
            }

            if(listaSusjednihVrhova.find(vrh3) == listaSusjednihVrhova.end()){
                listaSusjednihVrhova.insert(std::pair<int, std::vector<int>>(vrh3, std::vector<int>()));
                listaSusjednihVrhova[vrh3].push_back(i);
            }else{
                listaSusjednihVrhova[vrh3].push_back(i);
            }
        }

        for(i = 0; i < brojVrhova; i++){
            int brojSusjednihVrhova = listaSusjednihVrhova[i].size();
            GLfloat ukupnoX = 0;
            GLfloat ukupnoY = 0;
            GLfloat ukupnoZ = 0;
            for(int j = 0; j < brojSusjednihVrhova; j++){
                ukupnoX += normaleTrokuta[3 * listaSusjednihVrhova[i][j]];
                ukupnoY += normaleTrokuta[3 * listaSusjednihVrhova[i][j] + 1];
                ukupnoZ += normaleTrokuta[3 * listaSusjednihVrhova[i][j] + 2];
            }
            ukupnoX /= brojSusjednihVrhova;
            ukupnoY /= brojSusjednihVrhova;
            ukupnoZ /= brojSusjednihVrhova;

            glm::vec3 normala = glm::normalize(glm::vec3(ukupnoX, ukupnoY, ukupnoZ));

            normaleVrhova.push_back(normala.x);
            normaleVrhova.push_back(normala.y);
            normaleVrhova.push_back(normala.z);
        }
    }

    void spojiVrhoveINormale(){
        for(int i = 0; i < brojVrhova; i++){
            spojeniVrhoviINormale.push_back(vrhovi[3 * i]);
            spojeniVrhoviINormale.push_back(vrhovi[3 * i + 1]);
            spojeniVrhoviINormale.push_back(vrhovi[3 * i + 2]);
            spojeniVrhoviINormale.push_back(normaleVrhova[3 * i]);
            spojeniVrhoviINormale.push_back(normaleVrhova[3 * i + 1]);
            spojeniVrhoviINormale.push_back(normaleVrhova[3 * i + 2]);
        }
    }

};

class Voda : public Model
{
public:
    virtual void prepare()
    {
        sjencar = new Sjencar(vertexSjencarVodaPut, fragmentSjencarVodaPut);
        glGenVertexArrays(1, &vaoId);
        glBindVertexArray(vaoId);
        glGenBuffers(1, &vboId);
        glBindBuffer(GL_ARRAY_BUFFER, vboId);
        glBufferData(GL_ARRAY_BUFFER, vrhovi.size() * sizeof(GLfloat), &vrhovi[0], GL_STATIC_DRAW);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void *)0);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void *)(3* sizeof(float)));
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);
        glBindVertexArray(0);
    }

    virtual void draw()
    {
        sjencar->koristi();
        sjencar->setMat4("projekcijskaMatrica", projekcijskaMatrica);
        sjencar->setMat4("viewMatrica", viewMatrica);
        sjencar->setMat4("modelMatrica", modelMatrica);
        sjencar->setInt("refrakcija", 0);
        sjencar->setInt("refleksija", 1);
        sjencar->setInt("dudvmap", 2);
        sjencar->setInt("dubinskaMapa", 3);
        sjencar->setInt("mapaNormala", 4);
        sjencar->setFloat("vrijeme", vrijeme);
        sjencar->setVec3("pozicija_kamere", kamera.ociste);
        sjencar->setVec3("pozicijaSunca", pozicija_sunca);
        glBindVertexArray(vaoId);
        glDrawArrays(GL_TRIANGLES, 0, brojVrhova);
        glBindVertexArray(0);
    }

    virtual void clean()
    {
        glDeleteBuffers(1, &vboId);
        glDeleteVertexArrays(1, &vaoId);
    }

    void update(float dt){
        vrijeme += dt;
    }
private:
    GLuint vaoId;
    GLuint vboId;
    Sjencar *sjencar;
    GLint brojVrhova = 6;
    std::vector<GLfloat> vrhovi = {
        //  x      y             z       u       v
            20.0, visinaVode,  20.0,   1.0,    1.0,
            20.0, visinaVode, -20.0,   1.0,    0.0,
            -20.0, visinaVode, -20.0,  0.0,    0.0,
            -20.0, visinaVode, -20.0,  0.0,    0.0,
            -20.0, visinaVode, 20.0,   0.0,    1.0,
            20.0, visinaVode, 20.0,    1.0,    1.0
    };
    GLfloat vrijeme = 0.0f;
};

RootSceneGraphNode root;

std::vector<struct Cestica> cestice;
GLuint cesticaTexturaId;
#define BROJCESTICA 100

int main(int argc, char **argv)
{
    prozor.width = 300;
    prozor.height = 300;

    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
    glutInitWindowSize(prozor.width, prozor.height);
    glutInitWindowPosition(100, 100);
    glutInit(&argc, argv);

    prozor.windowID = glutCreateWindow("Treci labos");

    GLenum err = glewInit();
    if (GLEW_OK != err)
    {
        fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
    }

    init();

    glutReshapeFunc(myReshape);
    glutDisplayFunc(myDisplay);
    glutKeyboardFunc(myKeyboard);
    glutTimerFunc(16, iduciFrame, 0);
    glutMouseFunc(mouse_func);
    glutMotionFunc(motion_func);
    glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);
    glutMainLoop();
    root.cleanUp();
    std::cout << "Uspjesno izasao\n";
    return 0;
}


class Texture
{
public:
    GLuint textureID;
    Texture(const char *putDoTeksture);
};

Texture::Texture(const char *putDoTeksture)
{
    glGenTextures(1, &textureID);
    glBindTexture(GL_TEXTURE_2D, textureID);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    int width, height, nrChannels;
    unsigned char *data = stbi_load(putDoTeksture, &width, &height, &nrChannels, 0);
    if (data)
    {
        if (nrChannels == 3)
        {
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
            glGenerateMipmap(GL_TEXTURE_2D);
        }
        else if (nrChannels == 4)
        {
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
            glGenerateMipmap(GL_TEXTURE_2D);
        }
    }
    else
    {
        std::cout << "Failed to load texture" << std::endl;
    }
    stbi_image_free(data);
}

Voda voda;

void init()
{

    //initCestica();

    initVodaBufferi();

    kamera = {glm::vec3(0.0, 7.0, 13.0),
              glm::vec3(0.0, 5.0, 9.0),
              glm::vec3(0.0, 1.0, 0.0)};

    root.addChild(new ModelSceneGraphNode(new Podloga()));
    TranslateSceneGraphNode *translate = new TranslateSceneGraphNode(8, 6, 0);
    RotatedAnimatedSceneGraphNode *rotate = new RotatedAnimatedSceneGraphNode(0, 1, 0, 90, 36);
    ModelSceneGraphNode *model = new ModelSceneGraphNode(new UcitaniModel("modeli/aircraft747.obj"));
    ScaledSceneGraphNode *scaled = new ScaledSceneGraphNode(7, 7, 7);
    scaled->addChild(model);
    translate->addChild(scaled);
    rotate->addChild(translate);
    root.addChild(rotate);

    UcitaniModel *stablo = new UcitaniModel("modeli/tree.obj", vertexSjencarTreePut, fragmentSjencarTreePut);
    ModelSceneGraphNode *stablonode = new ModelSceneGraphNode(stablo);
    RotatedSceneGraphNode *stabloRotated = new RotatedSceneGraphNode(1,0,0, -90);
    stabloRotated->addChild(stablonode);
    ScaledSceneGraphNode *scaledStablo = new ScaledSceneGraphNode(7,7,7);
    scaledStablo->addChild(stabloRotated);
    TranslateSceneGraphNode *stablotrans1 = new TranslateSceneGraphNode(12, 5.5, 5);
    stablotrans1->addChild(scaledStablo);
    root.addChild(stablotrans1);

    TranslateSceneGraphNode *stablotrans2 = new TranslateSceneGraphNode(15, 5.5, 10);
    stablotrans2->addChild(scaledStablo);
    root.addChild(stablotrans2);

    TranslateSceneGraphNode *stablotrans3 = new TranslateSceneGraphNode(15, 5.5, -10);
    stablotrans3->addChild(scaledStablo);
    root.addChild(stablotrans3);

    TranslateSceneGraphNode *stablotrans4 = new TranslateSceneGraphNode(-15, 5.5, 0);
    stablotrans4->addChild(scaledStablo);
    root.addChild(stablotrans4);

    TranslateSceneGraphNode *stablotrans5 = new TranslateSceneGraphNode(-5, 5.5, -15);
    stablotrans5->addChild(scaledStablo);
    root.addChild(stablotrans5);


    root.prepare();

    voda.prepare();

    Texture vodaDuDv = Texture("teksture/waterDUDV.png");
    dudvmapId = vodaDuDv.textureID;

    Texture mapaNormala = Texture("teksture/matchingNormalMap.png");
    mapaNormalaId = mapaNormala.textureID;

}

void initCestica(){
    Texture t = Texture("teksture/snow.bmp");
    cesticaTexturaId = t.textureID;

    for (int i = 0; i < BROJCESTICA; i++)
    {
        struct Cestica cestica;
        cestica.pozx = random_double(-20, 20);
        cestica.pozy = random_double(0, 10);
        cestica.pozz = random_double(-20, 20);
        cestica.zivot = random_double(-1, 1);
        cestica.vx = random_double(-0.2, 0.2);
        cestica.vy = random_double(-0.2, 0.2);
        cestica.vz = random_double(-0.2, 0.2);
        cestice.push_back(cestica);
    }
}

void drawCestica()
{
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, cesticaTexturaId);
    glm::vec3 ociste = kamera.ociste;
    glDepthMask(GL_FALSE);
    glUseProgram(0);
    for (auto cestica : cestice)
    {
        glm::vec3 pozicija = glm::vec3(cestica.pozx, cestica.pozy, cestica.pozz);
        glColor4f(1, 1, 1, 1 - fabs(cestica.zivot));
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glTranslatef(cestica.pozx, cestica.pozy, cestica.pozz);
        glm::vec3 s = glm::vec3(0, 1, 0);
        glm::vec3 e = ociste - pozicija;
        glm::vec3 os = glm::cross(s, e);
        double cosfi = glm::dot(s, e) / glm::length(e);
        double fi = radToDeg(acos(cosfi));
        glRotatef(fi, os.x, os.y, os.z);
        glEnable(GL_TEXTURE_2D);
        glBegin(GL_TRIANGLE_FAN);
        glTexCoord2f(1, 1);
        glVertex3f(1 - fabs(cestica.zivot), 0, 1 - fabs(cestica.zivot));
        glTexCoord2f(1, 0);
        glVertex3f(1 - fabs(cestica.zivot), 0, -(1 - fabs(cestica.zivot)));
        glTexCoord2f(0, 0);
        glVertex3f(-(1 - fabs(cestica.zivot)), 0, -(1 - fabs(cestica.zivot)));
        glTexCoord2f(0, 1);
        glVertex3f(-(1 - fabs(cestica.zivot)), 0, 1 - fabs(cestica.zivot));
        glEnd();
        glDisable(GL_TEXTURE_2D);
        glPopMatrix();
    }
    glDepthMask(GL_TRUE);
}

void updateCestica(double dt)
{
    for (auto iter = cestice.begin(); iter != cestice.end(); ++iter)
    {
        if ((*iter).zivot < -1)
        {
            (*iter).pozx = random_double(-20, 20);
            (*iter).pozy = random_double(0, 10);
            (*iter).pozz = random_double(-20, 20);
            (*iter).zivot = 1;
            (*iter).vx = random_double(-0.2, 0.2);
            (*iter).vy = random_double(-0.2, 0.2);
            (*iter).vz = random_double(-0.2, 0.2);
        }
        (*iter).pozx += (*iter).vx * dt;
        (*iter).pozy += (*iter).vy * dt;
        (*iter).pozz += (*iter).vz * dt;
        (*iter).zivot -= dt * 0.1;
    }
}

void myDisplay(void)
{
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    modelMatrica = glm::mat4(1.0f);
    viewMatrica = glm::lookAt(kamera.ociste,
                            kamera.glediste,
                            kamera.vup);
    projekcijskaMatrica = glm::perspective(45.0f, (float)prozor.width / prozor.height, 0.5f, 1000.0f);

    gluLookAt(kamera.ociste.x, kamera.ociste.y, kamera.ociste.z,
              kamera.glediste.x, kamera.glediste.y, kamera.glediste.z,
              kamera.vup.x, kamera.vup.y, kamera.vup.z);
    glEnable(GL_DEPTH_TEST);
    //glEnable(GL_CULL_FACE);
    glEnable(GL_CLIP_DISTANCE0);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    glClearColor((float)135 / 255, (float)206 / 255, (float)235 / 255, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glPointSize(1.0);
    glColor3f(0.0f, 0.0f, 0.0f);
    crtaj();
    glutSwapBuffers();
}

void myReshape(int w, int h)
{
    prozor.width = w;
    prozor.height = h;
    glViewport(0, 0, prozor.width, prozor.height);
    //glClearColor(1,1,1,0);
    glClearColor((float)135 / 255, (float)206 / 255, (float)235 / 255, 0.0f); // boja pozadine
    //glClearColor(0,0,0,0); //crna pozadina
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glPointSize(1.0);
    glColor3f(0.0f, 0.0f, 0.0f);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0, (float)prozor.width / prozor.height, 0.5, 1000.0);
    // kut pogleda, x/y, prednja i straznja ravnina odsjecanja
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(kamera.ociste.x, kamera.ociste.y, kamera.ociste.z,
              kamera.glediste.x, kamera.glediste.y, kamera.glediste.z,
              kamera.vup.x, kamera.vup.y, kamera.vup.z);
    // ociste x,y,z
    // glediste x,y,z
    // up vektor x,y,z

    modelMatrica = glm::mat4(1.0f);
    viewMatrica = glm::lookAt(kamera.ociste,
                            kamera.glediste,
                            kamera.vup);
    projekcijskaMatrica = glm::perspective(45.0f, (float)prozor.width / prozor.height, 0.5f, 1000.0f);
}

void myKeyboard(unsigned char theKey, int mouseX, int mouseY)
{
    //dodati
}

void crtaj()
{

    glClearColor((float)135 / 255, (float)206 / 255, (float)235 / 255, 0.0f);
    static int inicijaliziraniSat;
    static std::chrono::_V2::system_clock::time_point t1;
    static std::chrono::_V2::system_clock::time_point t2;
    if (!inicijaliziraniSat)
    {
        inicijaliziraniSat = 1;
        t1 = std::chrono::high_resolution_clock::now();
    }
    t2 = t1;
    t1 = std::chrono::high_resolution_clock::now();
    double trajanje = static_cast<double>(std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t2).count()) / 1000.0;

    //std::cout << "trajanje" << trajanje << "\n";

    clip_ravnina = -1;
    root.update(trajanje);
    root.draw();

    glUseProgram(0);
    glBegin(GL_LINES);
    glColor3f(1, 0, 0);
    glVertex3f(kamera.glediste.x, kamera.glediste.y, kamera.glediste.z);
    glVertex3f(kamera.glediste.x + 1, kamera.glediste.y, kamera.glediste.z);
    glColor3f(0, 1, 0);
    glVertex3f(kamera.glediste.x, kamera.glediste.y, kamera.glediste.z);
    glVertex3f(kamera.glediste.x, kamera.glediste.y + 1, kamera.glediste.z);
    glColor3f(0, 0, 1);
    glVertex3f(kamera.glediste.x, kamera.glediste.y, kamera.glediste.z);
    glVertex3f(kamera.glediste.x, kamera.glediste.y, kamera.glediste.z + 1);
    glEnd();

    glBegin(GL_TRIANGLES);
    glVertex3f(pozicija_sunca.x, pozicija_sunca.y, pozicija_sunca.z);
    glVertex3f(pozicija_sunca.x + 5, pozicija_sunca.y, pozicija_sunca.z);
    glVertex3f(pozicija_sunca.x, pozicija_sunca.y, pozicija_sunca.z + 5);
    glEnd();

    glClearColor(0,0,0,0); 

    //refrakcija ide u texture unit 0
    clip_ravnina = 0;
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, refrakcijaFrameBuffer);
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
	glViewport(0, 0, sirina_refrakcije, visina_refrakcije);
    root.draw();
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, prozor.width, prozor.height);

    //refleksija ide u testure unit 1
    clip_ravnina = 1;
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, reflekcijaFrameBuffer);
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
	glViewport(0, 0, sirina_refleksije, visina_refleksije);
    glm::mat4 spremljenaView = viewMatrica;
    double ociste_iznad_vode = 2 * (kamera.ociste.y - visinaVode);
    double glediste_iznad_vode = 2 * (kamera.glediste.y - visinaVode);
    glm::vec3 novo_ociste = kamera.ociste - glm::vec3(0, ociste_iznad_vode, 0);
    glm::vec3 novo_glediste = kamera.glediste - glm::vec3(0, glediste_iznad_vode, 0);
    viewMatrica = glm::lookAt(novo_ociste, novo_glediste, kamera.vup);
    root.draw();
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, prozor.width, prozor.height);


    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, refrakcijaTekstura);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, refleksijaTekstura);
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, dudvmapId);
    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D, refrakcijaDepthTekstura);
    glActiveTexture(GL_TEXTURE4);
    glBindTexture(GL_TEXTURE_2D, mapaNormalaId);

    clip_ravnina = -1;
    //vrati staru view matricu
    viewMatrica = spremljenaView;
    voda.update(trajanje);
    voda.draw();

    //updateCestica(trajanje);
    //drawCestica();
}

int old_x = 0;
int old_y = 0;
int valid = 0;
int mode = 0;

void mouse_func(int button, int state, int x, int y)
{
    old_x = x;
    old_y = y;
    if (button == GLUT_LEFT_BUTTON)
    {
        mode = 1;
        valid = state == GLUT_DOWN;
    }
    else if (button == GLUT_RIGHT_BUTTON)
    {
        mode = -1;
        valid = state == GLUT_DOWN;
    }
    else if (button == 3 && state == GLUT_DOWN)
    {
        //std::cout << "kotac gore\n";
        zoom(1);
    }
    else if (button == 4 && state == GLUT_DOWN)
    {
        //std::cout << "kotac dolje\n";
        zoom(-1);
    }
}

void zoom(int dir)
{
    glm::vec3 pomak = kamera.glediste - kamera.ociste;
    glm::vec3 ociste = kamera.ociste;
    glm::vec3 glediste = kamera.glediste;
    if (dir == 1)
    {
        glediste = glediste + pomak;
        ociste = ociste + pomak;
    }
    else if (dir == -1)
    {
        glediste = glediste - pomak;
        ociste = ociste - pomak;
    }

    kamera.glediste = glediste;
    kamera.ociste = ociste;
    glutPostRedisplay();
}

void motion_func(int x, int y)
{
    if (valid)
    {
        int dx = old_x - x;
        int dy = old_y - y;
        updatePerspective(dx, dy);
    }
    old_x = x;
    old_y = y;
}

void updatePerspective(int dx, int dy)
{
    if (mode == 1)
    {

        glm::vec3 vup = glm::vec3(0, 1, 0);
        glm::vec3 vektor = kamera.glediste - kamera.ociste;
        glm::vec3 rezultat = glm::vec3(0);
        rezultat += vektor * (float)cos(degToRad(dx));
        rezultat += glm::cross(vektor, vup) * (float)sin(degToRad(dx));
        rezultat += vup * vektor * vup * (float)(1 - cos(degToRad(dx)));

        kamera.glediste = kamera.ociste + rezultat;

        vup = glm::vec3(1, 0, 0);
        vektor = kamera.glediste - kamera.ociste;
        rezultat = glm::vec3(0);
        rezultat += vektor * (float)cos(degToRad(dy));
        rezultat += glm::cross(vektor, vup) * (float)sin(degToRad(dy));
        rezultat += vup * vektor * vup * (float)(1 - cos(degToRad(dy)));

        kamera.glediste = kamera.ociste + rezultat;
    }
    else if (mode == -1)
    {
        glm::vec3 vup = glm::vec3(0, 1, 0);
        glm::vec3 vektor = kamera.ociste - kamera.glediste;
        glm::vec3 rezultat = glm::vec3(0);
        rezultat += vektor * (float)cos(degToRad(dx));
        rezultat += glm::cross(vektor, vup) * (float)sin(degToRad(dx));
        rezultat += vup * vektor * vup * (float)(1 - cos(degToRad(dx)));

        kamera.ociste = kamera.glediste + rezultat;

        vup = glm::vec3(1, 0, 0);
        vektor = kamera.ociste - kamera.glediste;
        rezultat = glm::vec3(0);
        rezultat += vektor * (float)cos(degToRad(dy));
        rezultat += glm::cross(vektor, vup) * (float)sin(degToRad(dy));
        rezultat += vup * vektor * vup * (float)(1 - cos(degToRad(dy)));


        kamera.ociste = kamera.glediste + rezultat;
    }
    glutPostRedisplay();
}

void iduciFrame(int broj)
{
    if (glutGetWindow() == 0)
    {
        return;
    }

    double msDoIduceg = static_cast<double>(1000) / 60;

    glutPostRedisplay();
    glutTimerFunc(msDoIduceg, iduciFrame, 0);
}

std::vector<GLfloat> razdijeliPodlogu(std::vector<GLfloat> *original)
{
    std::vector<GLfloat> *novi = new std::vector<GLfloat>();
    auto iter = original->begin();
    while (iter < original->end())
    {
        GLfloat x = *iter;
        iter++;
        GLfloat y = *iter;
        iter++;
        GLfloat z = *iter;
        glm::vec3 prvi = glm::vec3(x, y, z);
        iter++;
        x = *iter;
        iter++;
        y = *iter;
        iter++;
        z = *iter;
        iter++;
        glm::vec3 drugi = glm::vec3(x, y, z);
        x = *iter;
        iter++;
        y = *iter;
        iter++;
        z = *iter;
        iter++;
        glm::vec3 treci = glm::vec3(x, y, z);
        glm::vec3 cetvrti = (prvi + drugi) / 2.0f + glm::vec3(0, +1, 0);
        glm::vec3 peti = (drugi + treci) / 2.0f + glm::vec3(0, +1, 0);
        glm::vec3 sesti = (prvi + treci) / 2.0f + glm::vec3(0, -1, 0);

        novi->push_back(prvi.x);
        novi->push_back(prvi.y);
        novi->push_back(prvi.z);
        novi->push_back(cetvrti.x);
        novi->push_back(cetvrti.y);
        novi->push_back(cetvrti.z);
        novi->push_back(sesti.x);
        novi->push_back(sesti.y);
        novi->push_back(sesti.z);

        novi->push_back(peti.x);
        novi->push_back(peti.y);
        novi->push_back(peti.z);
        novi->push_back(sesti.x);
        novi->push_back(sesti.y);
        novi->push_back(sesti.z);
        novi->push_back(cetvrti.x);
        novi->push_back(cetvrti.y);
        novi->push_back(cetvrti.z);

        novi->push_back(cetvrti.x);
        novi->push_back(cetvrti.y);
        novi->push_back(cetvrti.z);
        novi->push_back(drugi.x);
        novi->push_back(drugi.y);
        novi->push_back(drugi.z);
        novi->push_back(peti.x);
        novi->push_back(peti.y);
        novi->push_back(peti.z);

        novi->push_back(sesti.x);
        novi->push_back(sesti.y);
        novi->push_back(sesti.z);
        novi->push_back(peti.x);
        novi->push_back(peti.y);
        novi->push_back(peti.z);
        novi->push_back(treci.x);
        novi->push_back(treci.y);
        novi->push_back(treci.z);
    }
    return *novi;
}

void initVodaBufferi(){
    glGenFramebuffers(1, &refrakcijaFrameBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, refrakcijaFrameBuffer);
    glDrawBuffer(GL_COLOR_ATTACHMENT0);

    glGenTextures(1, &refrakcijaTekstura);
    glBindTexture(GL_TEXTURE_2D, refrakcijaTekstura);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, sirina_refrakcije, visina_refrakcije,
     0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, refrakcijaTekstura, 0);

    glGenTextures(1, &refrakcijaDepthTekstura);
	glBindTexture(GL_TEXTURE_2D, refrakcijaDepthTekstura);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, sirina_refrakcije, visina_refrakcije,
				0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, refrakcijaDepthTekstura, 0);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, prozor.width, prozor.height);

    glGenFramebuffers(1, &reflekcijaFrameBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, reflekcijaFrameBuffer);
    glDrawBuffer(GL_COLOR_ATTACHMENT0);

    glGenTextures(1, &refleksijaTekstura);
    glBindTexture(GL_TEXTURE_2D, refleksijaTekstura);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, sirina_refleksije, visina_refleksije,
     0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, refleksijaTekstura, 0);

    glGenTextures(1, &refleksijaDepthTekstura);
	glBindTexture(GL_TEXTURE_2D, refleksijaDepthTekstura);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, sirina_refleksije, visina_refleksije,
				0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, refleksijaDepthTekstura, 0);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, prozor.width, prozor.height);
}

/*
int brojTrokuta;
int brojVrhova;
std::vector<GLfloat> vrhovi;
std::vector<GLfloat> trokuti;
std::vector<GLfloat> normaleVrhova;

void racunajRavnine(){
    int i;
    std::vector<GLfloat> normaleTrokuta;
    std::map<int,std::vector<int>> listaSusjednihVrhova;

    for(i = 0; i < brojTrokuta; i++){
        int vrh1 = vrhovi[3 * i];
        int vrh2 = vrhovi[3 * i + 1];
        int vrh3 = vrhovi[3 * i + 2];
        glm::vec3 v1 = glm::vec3(vrhovi[3 * vrh1], vrhovi[3 * vrh1 + 1], vrhovi[3 * vrh1 + 2]);
        glm::vec3 v2 = glm::vec3(vrhovi[3 * vrh2], vrhovi[3 * vrh2 + 1], vrhovi[3 * vrh2 + 2]);
        glm::vec3 v3 = glm::vec3(vrhovi[3 * vrh3], vrhovi[3 * vrh3 + 1], vrhovi[3 * vrh3 + 2]);

        glm::vec3 A = v2 - v1;
        glm::vec3 B = v3 - v1;
        glm::vec3 N = glm::cross(A,B);
        normaleTrokuta.push_back(N.x);
        normaleTrokuta.push_back(N.y);
        normaleTrokuta.push_back(N.z);
    }

    for(i = 0; i < brojTrokuta; i++){
        if(listaSusjednihVrhova.find(vrhovi[3 * i]) == listaSusjednihVrhova.end()){
            listaSusjednihVrhova.insert(std::pair<int, std::vector<int>>(vrhovi[3 * i], std::vector<int>()));
            listaSusjednihVrhova[vrhovi[3 * i]].push_back(i);
        }else{
            listaSusjednihVrhova[vrhovi[3 * i]].push_back(i);
        }

        if(listaSusjednihVrhova.find(vrhovi[3 * i + 1]) == listaSusjednihVrhova.end()){
            listaSusjednihVrhova.insert(std::pair<int, std::vector<int>>(vrhovi[3 * i + 1], std::vector<int>()));
            listaSusjednihVrhova[vrhovi[3 * i + 1]].push_back(i);
        }else{
            listaSusjednihVrhova[vrhovi[3 * i + 1]].push_back(i);
        }

        if(listaSusjednihVrhova.find(vrhovi[3 * i + 2]) == listaSusjednihVrhova.end()){
            listaSusjednihVrhova.insert(std::pair<int, std::vector<int>>(vrhovi[3 * i + 2], std::vector<int>()));
            listaSusjednihVrhova[vrhovi[3* i + 2]].push_back(i);
        }else{
            listaSusjednihVrhova[vrhovi[3 * i + 2]].push_back(i);
        }
    }
    for(i = 0; i < brojVrhova; i++){
        int brojSusjednihVrhova = listaSusjednihVrhova[i].size();
        GLfloat ukupnoX = 0;
        GLfloat ukupnoY = 0;
        GLfloat ukupnoZ = 0;
        for(int j = 0; j < brojSusjednihVrhova; j++){
            ukupnoX += normaleTrokuta[3 * listaSusjednihVrhova[i][j]];
            ukupnoY += normaleTrokuta[3 * listaSusjednihVrhova[i][j] + 1];
            ukupnoZ += normaleTrokuta[3 * listaSusjednihVrhova[i][j] + 2];
        }
        ukupnoX /= brojSusjednihVrhova;
        ukupnoY /= brojSusjednihVrhova;
        ukupnoZ /= brojSusjednihVrhova;

        glm::vec3 normala = glm::normalize(glm::vec3(ukupnoX, ukupnoY, ukupnoZ));

        normaleVrhova.push_back(normala.x);
        normaleVrhova.push_back(normala.y);
        normaleVrhova.push_back(normala.z);
    }

}
*/