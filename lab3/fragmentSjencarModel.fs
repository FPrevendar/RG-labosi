#version 330 core

out vec4 FragColor;
uniform vec4 color;
uniform vec3 pozicijaSunca;
in vec3 outPos;
in vec3 normala;

const vec3 boja_sunca = vec3(0.933, 0.909, 0.349);

void main()
{
    float k = 5.0 * dot(normala, normalize(pozicijaSunca - outPos));
    FragColor = vec4(0.5 * (normala/2.0 + 0.5), 1.0) * vec4(color.r * 3 * sin(outPos.x), 0.5 * color.g, color.b * 5 * cos(outPos.z), color.a);

    vec3 difuzna_komponenta = clamp((boja_sunca) * k, 0.0, 1.0);
    FragColor.rgb += difuzna_komponenta;
}
