#version 330 core

layout (location = 0) in vec3 vertexPos;
layout (location = 1) in vec2 uvcoords;

uniform mat4 modelMatrica;
uniform mat4 viewMatrica;
uniform mat4 projekcijskaMatrica;
uniform vec3 pozicija_kamere;

out vec2 uvCoords;
out vec4 pozicija_na_ekranu;
out vec3 outPos;

void main()
{
   vec4 pozicija_u_sceni = modelMatrica * vec4(vertexPos, 1);
   pozicija_na_ekranu = projekcijskaMatrica * viewMatrica * pozicija_u_sceni;
   gl_Position = pozicija_na_ekranu;
   uvCoords = uvcoords;
   outPos = (modelMatrica * vec4(vertexPos, 1.0)).xyz;
}
