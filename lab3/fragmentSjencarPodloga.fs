#version 330 core

out vec4 FragColor;
uniform vec4 color;
in vec3 outPos;

void main()
{
    FragColor = vec4(color.r * cos(outPos.y), color.g * outPos.y * 0.2, color.b * outPos.y * 0.2, color.a);
}
